package config;

import java.io.IOException;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import driver.SimpleDriver;
import driver.WebdriverFactory;
import properties.TestProperties;

//This class gives us a Thread Local Webdriver and initializes property file.
public class FrameworkContext {

	public static volatile FrameworkContext frameworkContext;

	private TestProperties prop;

	private FrameworkContext() {
		try {
			prop = new TestProperties();
		} catch (Exception e) {
			System.out.println("Error initializing framework" + e);
		}

	}

	ThreadLocal<WebDriver> driver = new ThreadLocal<>();

	public static FrameworkContext getInstance() {
		if (frameworkContext == null) {
			synchronized (FrameworkContext.class) {
				if (frameworkContext == null)
					frameworkContext = new FrameworkContext();
			}
		}
		return frameworkContext;
	}

	public TestProperties getTestProperties() {
		return prop;

	}

	public WebDriver getWebDriver() {
		if (driver.get() == null) {
			SimpleDriver simpledriver = null;
			try {
				simpledriver = WebdriverFactory.setupDriver();
			} catch (Exception e) {
				System.out.println("Error initializing driver");

			}
			driver.set(simpledriver.createWebDriver());
		}
		return driver.get();
	}

	public void removeWebDriver() {
		if (driver.get() != null) {
			driver.get().quit();
			driver.set(null);
		}
	}

}
