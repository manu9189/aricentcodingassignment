package restframework;

import java.util.List;

public class Winners {
	int winnerId;
	List<Integer> winNumbers;

	public int getWinnerId() {
		return winnerId;
	}

	public void setWinnerId(int winnerId) {
		this.winnerId = winnerId;
	}

	public List<Integer> getWinNumbers() {
		return winNumbers;
	}

	public void setWinNumbers(List<Integer> winNumbers) {
		this.winNumbers = winNumbers;
	}

}
