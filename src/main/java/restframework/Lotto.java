package restframework;

import java.util.List;
import java.util.Map;

public class Lotto {

	int lottoId;
	List<Integer> winningNumbers;
	List<Winners> winners;

	public int getLottoId() {
		return lottoId;
	}

	public void setLottoId(int lottoId) {
		this.lottoId = lottoId;
	}

	public List<Integer> getWinningNumbers() {
		return winningNumbers;
	}

	public void setWinningNumbers(List<Integer> winningNumbers) {
		this.winningNumbers = winningNumbers;
	}

	public List<Winners> getWinners() {
		return winners;
	}

	public void setWinners(List<Winners> winners) {
		this.winners = winners;
	}

}
