package properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.util.Properties;

public class TestProperties {

	private Properties p = new Properties();
	private InputStream s;

	public TestProperties() throws Exception {
		try {

			File propertyFile = new File(System.getProperty("user.dir") + "\\resources\\selenium.properties");
			s = new FileInputStream(propertyFile);
			p.load(s);

		} catch (Exception e) {
			System.out.println(e + "Error loading property file");
		} finally {
			s.close();
		}

	}

	public String getBrowserType() {
		return p.getProperty("browser");
	}

	public boolean isOnHub() {
		return p.getProperty("ongrid").equals("true");
	}

	public String getuserName() {
		return p.getProperty("username");

	}

	public String getPassword() {
		return p.getProperty("password");
	}

	public String getUrl() {
		return p.getProperty("loginpageurl");
	}

}
