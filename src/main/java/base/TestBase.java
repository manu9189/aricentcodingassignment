package base;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;

import config.FrameworkContext;
import pageobjects.LoginPageObject;

public class TestBase {

	@AfterMethod
	public void after(ITestResult result)

	{
		if (result.getStatus() == ITestResult.FAILURE) {
			result.getThrowable().getMessage();
		}
	}

	@AfterSuite(alwaysRun = true)
	public void teardown() {
		FrameworkContext.getInstance().removeWebDriver();
	}

	public boolean validateLogin() {
		LoginPageObject page = new LoginPageObject();
		page.get();
		page.login(FrameworkContext.getInstance().getTestProperties().getuserName(),
				FrameworkContext.getInstance().getTestProperties().getPassword());
		return page.verifyElementPresent("//p[text()=' Logged in Successfully ']");
	}
}
