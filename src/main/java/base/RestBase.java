package base;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import io.restassured.response.Response;
import restframework.Lotto;
import restframework.Winners;

public class RestBase {

	public Lotto getObjectFromResponse(Response response) {
		Lotto lotto = response.getBody().as(Lotto.class);
		return lotto;

	}

	public static String getBodyFromJsonFile(String filePath) throws IOException {

		return new String(Files.readAllBytes(Paths.get(filePath)));

	}

	public boolean validateLotteryId(Lotto lotto) {
		return (lotto.getLottoId() == 5);
	}

	public boolean validateLotteryNumbers(Lotto lotto, int id) {
		int[] compare = { 34, 23, 5 };
		int count = 0;
		List<Winners> winners = lotto.getWinners();
		for (Winners win : winners) {
			if (win.getWinnerId() == id) {
				Set<Integer> winningNumbers = new HashSet<>(win.getWinNumbers());
				for (int i = 0; i < compare.length; i++) {
					if (winningNumbers.contains(compare[i])) {
						count++;
					}
				}
			}
		}
		return (count == 3);

	}

}
