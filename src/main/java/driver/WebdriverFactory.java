package driver;

import config.FrameworkContext;
import driver.SimpleDriver;
import properties.TestProperties;

public class WebdriverFactory {

	public static SimpleDriver setupDriver() {
		// TODO Auto-generated method stub
		TestProperties testProperties = FrameworkContext.getInstance().getTestProperties();
		SimpleDriver driver = null;
		if (testProperties.isOnHub())
			driver = new Grid();
		if (testProperties.getBrowserType().equals("firefox")) {
			driver = new Firefox();
		}
		if (testProperties.getBrowserType().equals("chrome"))
			driver = new Chrome();
		return driver;
	}

}
