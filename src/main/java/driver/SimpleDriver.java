package driver;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import config.FrameworkContext;
import properties.TestProperties;

public abstract class SimpleDriver {

	public abstract WebDriver getWebdriver();

	public WebDriver createWebDriver() {
		WebDriver driver = getWebdriver();
		driver.manage().deleteAllCookies();
		try {
			driver.manage().window().maximize();
		} catch (Exception e) {
			System.out.println("Error maximizing browser");
		}

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return driver;
	}

}
