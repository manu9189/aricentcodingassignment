package driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.github.bonigarcia.wdm.ChromeDriverManager;

public class Chrome extends SimpleDriver {

	@Override
	public WebDriver getWebdriver() {

		ChromeDriverManager.getInstance(ChromeDriver.class).setup();
		ChromeOptions c = new ChromeOptions();
		c.addArguments("--start-maximized");
		return new ChromeDriver(c);
	}

}
