package pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class HomePage extends BasePageObject {
	@FindBy(id = "login")
	private WebElement login;

	@Override
	protected void isLoaded() throws Error {
		Assert.assertTrue(getDriver().getTitle().equals("manu"));

	}

	@Override
	protected void load() {
		getDriver().get("https://manu.com");
		waitForPageLoad();

	}

	public void clickLoginBtn() {
		click(login, true);
		waitForPageLoad();
	}

}
