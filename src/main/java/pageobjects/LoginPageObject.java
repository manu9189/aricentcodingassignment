package pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class LoginPageObject extends BasePageObject {

	@FindBy(id = "okta-signin-username")
	private WebElement user;

	@FindBy(className = "okta-signin-password")
	private WebElement password;

	@FindBy(xpath = "//input[@class='okta-signin-password']/following-sibling::button")
	private WebElement signInBtn;

	@Override
	protected void isLoaded() throws Error {
		Assert.assertTrue(user.isDisplayed());

	}

	@Override
	protected void load() {
		HomePage page = new HomePage();
		page.get();
		waitForPageLoad();
		page.clickLoginBtn();

	}

	public void login(String username, String pass) {
		sendKeys(user, username);
		sendKeys(password, pass);
		click(signInBtn, true);
	}

}
