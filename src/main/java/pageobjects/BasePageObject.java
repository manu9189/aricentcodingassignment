package pageobjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;

import config.FrameworkContext;

public abstract class BasePageObject extends LoadableComponent<BasePageObject> {

	private WebDriver driver;

	public BasePageObject() {
		PageFactory.initElements(getDriver(), this);
	}

	public WebDriver getDriver() {
		if (driver == null) {
			driver = FrameworkContext.getInstance().getWebDriver();
		}
		return driver;

	}

	public Object executeScript(String script, Object... args) {
		return ((JavascriptExecutor) getDriver()).executeScript(script, args);
	}

	public void click(WebElement element, boolean... waitForElement) {
		if (waitForElement.length > 0) {
			waitForElement(element);
		}
		element.click();
	}

	public void switchToFrame(int index) {
		getDriver().switchTo().defaultContent();
		getDriver().switchTo().frame(index);
	}

	public void sendKeys(WebElement element, String s) {
		element.sendKeys(s);
	}

	public boolean verifyElementPresent(String path) {
		List<WebElement> a = getDriver().findElements(By.xpath("path"));
		return a.size() != 0;
	}

	public void waitForElement(WebElement element) {

		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void waitForPageLoad() {
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
			}
		};
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(pageLoadCondition);
	}

}
