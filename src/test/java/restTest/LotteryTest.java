package restTest;

import java.io.IOException;

import org.junit.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import base.RestBase;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import static io.restassured.RestAssured.given;

public class LotteryTest extends RestBase {

	public static RequestSpecBuilder builder;
	public static RequestSpecification requestSpec;

	@BeforeClass
	public static void setupRequestSpecBuilder() throws IOException {
		String token = "ABCDE12345ABCDE";
		builder.addCookie("value", true).addCookie("encode", false).addHeader("Authorization", "Bearer " + token);
		builder.setBody(getBodyFromJsonFile(System.getProperty("user.dir") + "\\resources\\postLottery.json"));
		requestSpec = builder.build();
		RestAssured.baseURI = "http://localhost:8080";
	}

	@Test
	public void validateLotteryPost() {
		// can also be done using hamcrest matchers
		Response response = given().spec(requestSpec).when().post("/lotto");
		Assert.assertTrue(validateLotteryId(getObjectFromResponse(response)));
		Assert.assertTrue(validateLotteryNumbers(getObjectFromResponse(response), 23));
	}

}
