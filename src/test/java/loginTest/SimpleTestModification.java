package loginTest;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

public class SimpleTestModification {

	private String param = "";

	@Factory(dataProvider = "getData")
	public SimpleTestModification(String param) {
		this.param = param;
	}

	@BeforeClass
	public void beforeClass() {
		System.out.println("Before class executed.");
	}

	@Test
	public void testMethod() {
		System.out.println("Value is: " + param);
	}

	@DataProvider
	public static Object[][] getData() {
		return new Object[][] { { "Apple" }, { "Bananna" }, { "Orange" } };
	}

}
